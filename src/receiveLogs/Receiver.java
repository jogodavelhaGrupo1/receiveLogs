package receiveLogs;

import com.rabbitmq.client.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Receiver {
	private final static String QUEUE_NAME = "mtech";

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.queueDeclare(QUEUE_NAME, true, false, false, null);
		//System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
					throws IOException {
				String message = new String(body, "UTF-8");
				SaveLog(message);
				//System.out.println(" [x] Received '" + message + "'");
			}
		};

		channel.basicConsume(QUEUE_NAME, true, consumer);
	}

	public static void SaveLog(String message) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter("log.txt", true));
		writer.write(message);
		writer.newLine();

		writer.close();
	}
}
